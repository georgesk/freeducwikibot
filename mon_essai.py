#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Un script très simple pour pywikibot, permettant d'injecter les pages
de la base de données de Primtux

les données relatives au site à modifier sont en partie dans le fichier 
/home/georgesk/.pywikibot/families/local_family.py, en partie dans
le fichier user-config.py et elles sont complétées par quelques lignes
ci-dessous : config2.register_families_folder(...) et
config.usernames[...

Exemple d'un essai :
--------------------

georgesk@georges:~/developpement/pywikibot$ python3 mon_essai.py
Sleeping for 9.5 seconds, 2021-02-12 23:43:21
Page [[local:fr:Essai]] saved
"""

import pywikibot
from pywikibot import config2, config
from datetime import datetime
import csv


def login (lang = "fr", family = "local",
           familyfolder = ".pywikibot/families/",
           user = 'Mediawiki georges'):
    """
    Connexion à un site fonctionnant à l'aide de mediawiki

    @param lang la langue utilisée ("fr" par défaut)
    @param family la famille de serveurs ("local" par défaut)
    @param familyfolder dossier où se trouvent les familles spéciales
           (par défaut : ".pywikibot/families/")
    @param user l'utilisateur servant à la connexion
    @return le site après connexion
    @rtype  pywikibot.Site
    """
    config2.register_families_folder(familyfolder)
    config.usernames[family][lang] = user
    site=pywikibot.Site(lang, family)
    site.login()
    return site

class FicheLogiciel:
    """
    Description d'une fiche de logiciel
    
    Le constructeur a les paramètres suivants :
    @param generateur : une fonction qui permet de renvoyer un résumé,
           puis un dictionnaire nom de propriété => valeur(s)
    """
    
    def __init__(self, generateur):
        self.nom = generateur("nom")
        self.resume = generateur("resume")
        self.proprietes = generateur("proprietes")
        return

    def __str__(self, style="plain"):
        return self.nom2mediawiki(style) + \
            self.resume2mediawiki(style) + \
            self.prop2mediawiki(style)

    def nom2mediawiki(self, style="plain"):
        return "== {nom} ==\n\n".format(nom = self.nom.strip())
    
    def resume2mediawiki(self, style="plain"):
        return "=== Résumé ===\n{resume}\n\n". format(resume = self.resume.strip())
    
    def prop2mediawiki(self, style="plain"):
        """
        exporte les propriétés au format mediawiki
        """
        template = "=== Propriétés ===\n{proprietes}\n\n"
        result = ""
        if style == "plain":
            for cle, valeur in self.proprietes.items():
                if type(valeur) == str:
                    result += "; {} : {}\n".format(cle, valeur)
                else:
                    result += "; {}\n".format(cle)
                    result += "".join([":* {}\n".format(item)
                                       for item in valeur])
        elif style == "table":
            result += "<table class='wikitable'>\n"
            # 1ère ligne du tableau
            result += "<tr><th class='hl1'>"
            result += "</th><th>".join(self.proprietes.keys())
            result += "</th></tr>\n"
            # 2ème ligne du tableau
            result += "<tr><td class='hl1'>"
            td=[]
            for cle, valeur in self.proprietes.items():
                if type(valeur) == str:
                    td.append(valeur)
                else:
                    td.append("<ul><li>{}</li></ul>".format(
                        "</li><li>".join(valeur)
                    ))
            result += "</td><td>".join(td)
            result += "</td></tr>\n"
            result += "</table>\n"
        return template.format(proprietes=result)
    
        

        
class FicheLogicielConteneur:
    """
    Cette classe permet de contenir des fiches de logiciels ; chaque fiche
    de logiciel comporte un contenu
    @parm fichierCsv un fichier où lire les données ("" par défaut)
    @param nomCle le nom de la colonne qui contient le nom (unique) de la fiche
    @param resumeCle le nom de la colonne qui contient le résumé de la fiche
    @param propListeCles noms des colonnes dont le contenu formera une liste
    @param style un style pour le mediawiki ("plain" par défaut, 
           "table" est aussi implémenté)
    """

    def __init__(self, fichierCsv="",
                 nomCle="", resumeCle="", propListeCles=[],
                 style="plain"):
        self.contenu = []
        self.style = style
        if fichierCsv:
            self.readCsv(fichierCsv, nomCle, resumeCle, propListeCles)
        return

    def __iter__(self):
        return FicheLogicielIterateur(self)

    def __str__(self):
        return "\n".join([fiche.__str__(self.style) for fiche in self])

    def readCsv(self, nomfichier, nomCle, resumeCle, propListeCles):
        """
        remplissage du conteneur à l'aide d'un fichier CSV
        @param nomfichier le chemin vers un fichier CSV
        @param nomCle le nom de la colonne qui contient le nom (unique)
               de la fiche
        @param resumeCle le nom de la colonne qui contient le résumé de la fiche
        @param propListeCles noms des colonnes dont le contenu formera une liste
        """
        def makeGenerateur(nomCle, resumeCle, propListeCles, champs, donnee):
            def generateur(letype):
                if letype == "nom":
                    return donnee[nomCle]
                if letype == "resume":
                    return donnee[resumeCle]
                elif letype == "proprietes":
                    dico = {}
                    for cle in champs:
                        if cle in (nomCle, resumeCle):
                            continue
                        if cle in propListeCles:
                            dico[cle] = [v.strip() for v in donnee[cle].split(',')]
                        else:
                            dico[cle] = donnee[cle]
                    return dico
                return
            return generateur
                
        with open(nomfichier) as infile:
            lecteur = csv.DictReader(infile)
            assert nomCle in lecteur.fieldnames
            assert resumeCle in lecteur.fieldnames
            for cle in propListeCles:
                assert cle in lecteur.fieldnames
            for donnee in lecteur:
                self.contenu.append(FicheLogiciel(makeGenerateur(nomCle, resumeCle, propListeCles, lecteur.fieldnames, donnee)))
        return

class FicheLogicielIterateur:

    def __init__(self, conteneur):
        self._conteneur = conteneur
        self._index = 0
        return

    def __next__(self):
        if self._index < len(self._conteneur.contenu):
            result = self._conteneur.contenu[self._index]
            self._index += 1
            return result
        else:
            raise StopIteration
        return
            
    

if __name__ == "__main__":
    flc = FicheLogicielConteneur(
        "primtux/logiciels-PrimTux-V6.csv",
        "LOGICIEL",
        "Descriptif",
        [
            "Domaine",
            "Matière",
            "Niveau",
            "Session PrimTux",
            "Licence",
        ],
        style = "table",
    )
    print(flc)
    site = login()
    page = pywikibot.Page(site, 'Essai')
    t = datetime.now()
    marquage = "    Cette page a été générée automatiquement par un script le {} à {}, voir https://framagit.org/georgesk/freeducwikibot\n\n".format(
        t.strftime("%d/%m %Y"), t.strftime("%H:%M")
    )
    page.text = marquage + str(flc)
    page.save()
                                    
