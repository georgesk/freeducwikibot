"""
petits ajouts locaux pour faciliter l'usage de la bibliothèque pywikibot

Copyright : (c) 2021 Georges Khaznadar <georgesk@debian.org>

Licence: GPL-3+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""
from pywikibot import config2, config, Site, FilePage
from pywikibot.specialbots import UploadRobot

def login (lang = "fr", family = "local",
           familyfolder = ".pywikibot/families/",
           user = 'Mediawiki georges'):
    """
    Connexion à un site fonctionnant à l'aide de mediawiki

    @param lang la langue utilisée ("fr" par défaut)
    @param family la famille de serveurs ("local" par défaut)
    @param familyfolder dossier où se trouvent les familles spéciales
           (par défaut : ".pywikibot/families/")
    @param user l'utilisateur servant à la connexion
    @return le site après connexion
    @rtype  pywikibot.Site
    """
    config2.register_families_folder(familyfolder)
    config.usernames[family][lang] = user
    site=Site(lang, family)
    site.login()
    return site

            
def upload(url, site,
           author="", publicationDate=None,
           prefix="Primtux-",
           description = "fichier téléchargé depuis ressources.primtux.fr",
           overwrite = False,
           loglevel = 0):
    """
    téléversement d'images ; si le paramètre overwrite est faux (par défaut),
       l'image n'est pas téléversée
    @param url l'URL d'une image
    @param site un site mediawiki accessible en écriture (obtenu avec login,
           voir ci-dessus)
    @type  site pywikibot.Site
    @param author l'auteur de l'image ("" par défaut)
    @param publicationDate la date de publication (None par défaut)
    @type  publicationDate datetime.datetime
    @param prefix un préfixe pour le début du nom de fichier
    @param description description, pour le champ ALT de l'image
    @param overwrite s'il est faux (par défaut),
       l'image n'est pas téléversée
    @param loglevel le niveau de verbosité
    """
    summary = "Importation de l'image par fichelogiciel/wikibot\n"
    if author:
        description += "; auteur = {}".format(author)
    if publicationDate:
        description += "; publié le {}".format(publicationDate)
    summary += description
    filename = "Fichier:"+prefix+url.split("/")[-1]
    f = FilePage(site, filename)
    if not overwrite:
        if f.exists():
            if loglevel >= 2:
                print("   {} existe déjà.".format(f))
            return
    ur = UploadRobot(
        url=url,
        description=description,
        filename_prefix=prefix,
        target_site=site,
        ignore_warning=True,
        always=True,
        summary=summary
    )
    ur.run()
    return
