#! /usr/bin/python3

"""
Une version ancienne de freeduc-book a été exportée vers HTML,
et elle a été récupérée par la commande : sh wget-freeduc-book.sh

Le fonctionnement de wget n'est pas très satisfaisant quand il
collabore en mode récursif avec la Wayback Machine, en effet le
--no-clobber ne semble pas marcher très fort, les fichiers .html
se trouvent renommés en .html.tmp.html (sinon c'est pire, ils sont
effacés après téléchargement), et les images sont dispersées dans
plusieurs répertoires autres que le répertoire attendu.

Le résultat du téléchargement se trouve dans le répertoire
web.archive.org/web/20081221175854/http%3A/ofset.sourceforge.net/freeduc/book/
mais aussi dans plusieurs autres répertoires plus ou moins voisins.

Le présent programme vise à rassembler tout cela dans un arbre un peu
plus rationnellement organisé.
"""
from subprocess import call, Popen, PIPE
import os
from bs4 import BeautifulSoup

BASE   = "web.archive.org"
RACINE = "web.archive.org/web/20081221175854/http%3A/ofset.sourceforge.net/freeduc/book"
SOMMAIRE = "book_10.html"

def copie_html_files():
    call("mkdir -p freeduc-book/images", shell=True)
    htmlFiles = os.listdir(RACINE)
    for f in htmlFiles:
        call("cp {}/{} {}/{}".format(RACINE,f,"freeduc-book",f),
             shell=True)

def rectifie_fichier(f, loglevel=0):
    if not f.endswith(".tmp.html"):
        return
    if loglevel > 0:
        print("{} --> {}".format(f, f.replace(".tmp.html","")))
    doc = BeautifulSoup(open("freeduc-book/" + f).read(), "html.parser")
    images = doc.find_all("img")
    if images :
        for i in images:
            path = i["src"].replace(":/","%3A")
            imgfile = os.path.basename(path)
            i["src"] = "images/" + imgfile
            call("cp {}/{} freeduc-book/images/{}".format(BASE, path, imgfile),
                 shell=True)
        with open("freeduc-book/"+f.replace(".tmp.html",""),"w") as target:
            target.write(doc.prettify())
    return
    
    
if __name__ == "__main__":
    copie_html_files()
    for f in os.listdir("freeduc-book"):
        rectifie_fichier(f, loglevel=1)
