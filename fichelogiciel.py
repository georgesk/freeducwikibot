#!/usr/bin/python3
"""
Structures pour traiter des fiches de logiciels éducatifs.

Copyright : (c) 2021 Georges Khaznadar <georgesk@debian.org>

Licence: GPL-3+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""
import csv, re, pathlib
import pywikibot
from datetime import datetime
from primtux_fiche import PrimtuxFiche
from wikibot import upload

VERSION = "1.4"
DEBUT_MARQUAGE = "Cette page a été générée automatiquement par un script"

class FicheLogiciel:
    """
    Description d'une fiche de logiciel
    
    Le constructeur a les paramètres suivants :
    @param generateur une fonction qui permet de renvoyer un résumé,
           puis un dictionnaire nom de propriété => valeur(s)
    @param site un site où on est logé
    @type  site pywikibot.Site
    """
    
    def __init__(self, generateur, site = None, loglevel = 0,
                 sources = []):
        self.nom = generateur("nom")
        self.resume = generateur("resume")
        self.proprietes = generateur("proprietes")
        self.site = site
        self.loglevel = loglevel
        self.sources = sources # liste de fichiers consultés pour la fiche
        return

    def __str__(self, style="plain"):
        """
        production d'une chaîne qui représente la fiche.
        @param style si c'est "plain" (valeur par défaut) alors le rendu
          est minimal ; sinon si c'est "table", il sera tabulaire avec une
          syntaxe mediawiki
        """
        return self.nom2mediawiki(style) + \
            self.resume2mediawiki(style) + \
            self.prop2mediawiki(style)

    def nom2mediawiki(self, style="plain"):
        return "== {nom} ==\n\n".format(nom = self.nom.strip())
    
    def resume2mediawiki(self, style="plain"):
        return "=== Résumé ===\n{resume}\n\n". format(resume = self.resume.strip())

    def guide(self, style="plain"):
        """
        examine self.proprietes ; s'il y a une fiche Primtux,
        calcule sa traduction en format Mediawiki
        @param style ("plain" par défaut)
        @return du texte au format mediawiki ; si style est "plain" alors
           c'est un texte très court, autrement c'est un guide plus fourni
        """
        if style == "plain":
            return self.proprietes["Fiche PrimTux"]
        wikiGuide = ""
        if "ressources.primtux.fr" in self.proprietes["Fiche PrimTux"]:
            self.sources.append(self.proprietes["Fiche PrimTux"])
            pf = PrimtuxFiche(self.proprietes["Fiche PrimTux"])
            wikiGuide = pf.html2wiki(pf.guide)
            for i in pf.images:
                self.sources.append(i)
                upload(
                    i, self.site,
                    author = pf.author,
                    publicationDate = pf.publicationDate,
                    prefix="Primtux-",
                    description = "fichier téléchargé depuis ressources.primtux.fr",
                    loglevel = self.loglevel
                )
        return wikiGuide
    
    def prop2mediawiki(self, style="plain"):
        """
        exporte les propriétés au format mediawiki
        """
        template = "=== Propriétés ===\n{proprietes}\n\n"
        result = ""
        if style == "plain":
            for cle, valeur in self.proprietes.items():
                if type(valeur) == str:
                    result += "; {} : {}\n".format(cle, valeur)
                else:
                    result += "; {}\n".format(cle)
                    result += "".join([":* {}\n".format(item)
                                       for item in valeur])
            return template.format(proprietes=result)
        elif style == "table":
            guide = self.guide(style=style)
            result += "<table class='wikitable'>\n"
            for key, val in self.proprietes.items():
                if val == ["-"]:
                    # on évite une liste d'une seule valeur non spécifiée
                    val = "-"
                elif type(val) != str:
                    # on recode la liste éventuelle en HTML
                    val = "<ul><li>{}</li></ul>".format(
                        "</li><li>".join(val))
                result += """
  <tr>
    <th class='hl1'>{}</th><td class='hl1'>{}</td>
  </tr>""".format(key, val)
            result += "</table>\n"
            result = template.format(proprietes=result)
        if guide:
            result += "== Guide de démarrage ==\n{}\n".format(guide)
        return result
    
        

        
class FicheLogicielIterateur:

    def __init__(self, conteneur):
        self._conteneur = conteneur
        self._index = 0
        return

    def __next__(self):
        if self._index < len(self._conteneur.contenu):
            result = self._conteneur.contenu[self._index]
            self._index += 1
            return result
        else:
            raise StopIteration
        return

class FicheLogicielConteneur:
    """
    Cette classe permet de contenir des fiches de logiciels ; chaque fiche
    de logiciel comporte un contenu
    @parm fichierCsv un fichier où lire les données ("" par défaut)
    @param nomCle le nom de la colonne qui contient le nom (unique) de la fiche
    @param resumeCle le nom de la colonne qui contient le résumé de la fiche
    @param propListeCles noms des colonnes dont le contenu formera une liste
    @param style un style pour le mediawiki ("plain" par défaut, 
           "table" est aussi implémenté)
    @param site un site mediawiki (None par défaut)
    @type  site pywikibot.Site
    @param loglevel niveau de journalisation; 0 par défaut ; à partir de 10
           on est en mode débogage allégé (les actions longues ne sont
           pas réalisées.
    """

    def __init__(self, fichierCsv="",
                 nomCle="", resumeCle="", propListeCles=[],
                 style="plain", site = None, loglevel = 0):
        self.contenu = []
        self.style = style
        self.site = site
        self.loglevel = loglevel
        p = pathlib.Path(fichierCsv)
        self.csvDate = datetime.fromtimestamp(p.stat().st_mtime)
        if fichierCsv:
            self.readCsv(fichierCsv, nomCle, resumeCle, propListeCles)
        return

    def __iter__(self):
        return FicheLogicielIterateur(self)

    def __str__(self):
        return "\n".join([fiche.__str__(style="plain") for fiche in self])

    def readCsv(self, nomfichier, nomCle, resumeCle, propListeCles):
        """
        remplissage du conteneur à l'aide d'un fichier CSV
        @param nomfichier le chemin vers un fichier CSV
        @param nomCle le nom de la colonne qui contient le nom (unique)
               de la fiche
        @param resumeCle le nom de la colonne qui contient le résumé de la fiche
        @param propListeCles noms des colonnes dont le contenu formera une liste
        """
        def makeGenerateur(nomCle, resumeCle, propListeCles, champs, donnee):
            def generateur(letype):
                if letype == "nom":
                    return donnee[nomCle]
                if letype == "resume":
                    return donnee[resumeCle]
                elif letype == "proprietes":
                    dico = {}
                    for cle in champs:
                        if cle in (nomCle, resumeCle):
                            continue
                        if cle in propListeCles:
                            dico[cle] = [v.strip() for v in donnee[cle].split(',')]
                        else:
                            dico[cle] = donnee[cle]
                    return dico
                return
            return generateur
                
        with open(nomfichier) as infile:
            lecteur = csv.DictReader(infile)
            assert nomCle in lecteur.fieldnames
            assert resumeCle in lecteur.fieldnames
            for cle in propListeCles:
                assert cle in lecteur.fieldnames
            for donnee in lecteur:
                self.contenu.append(FicheLogiciel(makeGenerateur(nomCle, resumeCle, propListeCles, lecteur.fieldnames, donnee), site=self.site, loglevel=self.loglevel, sources = [nomfichier]))
        return

    def siteUnePage(self, nomPage):
        """
        exporte tout le conteneur dans une page du mediawiki
        @param nomPage le nom de la page à créer
        """
        if not self.site:
            return
        page = pywikibot.Page(self.site, nomPage)
        t = datetime.now()
        page.text = self.marquage(sources=["https://ressources.primtux.fr/quel-logiciel-pour-quel-domaine-les-cartes-heuristiques/"]) + str(self)
        page.save()
        return

    def sitePlusieursPages(self, espaceNommage):
        """
        exporte le conteneur dans plusieurs pages
        @param espaceNommage un espace de nommage pour le wiki
        """
        if not self.site:
            return
        # on vérifie que les noms soient bien des clés uniques
        noms = [fiche.nom for fiche in self]
        ensemble = set(noms)
        if len(ensemble) != len(noms):
            from collections import Counter
            redondants = [n for (n,v) in Counter(noms).iteritems() if v > 1]
            raise Exception("Un des noms de fiche est redondant : {}".format(
                ", ".join(redondants)
            ))
        ###################### le sommaire
        if self.loglevel > 0:
            print("On réalise la page du sommaire")
        nomPage = "{}:{}".format(espaceNommage, "Sommaire")
        page = pywikibot.Page(self.site, nomPage)
        if self.estModifiableAuto(page):
            t = datetime.now()
            liens=["* [[{espace}:{nom}|{nom}]]".format(
                espace = espaceNommage, nom = fiche.nom) for fiche in self]
            page.text = self.marquage(sources=["https://ressources.primtux.fr/quel-logiciel-pour-quel-domaine-les-cartes-heuristiques/"]) + """\
    == Sommaire des fiches de logiciels éducatifs pour {} ==
    {}
    """.format(espaceNommage, "\n".join(liens))
            page.save()
        ##################### les autres pages
        if self.loglevel > 1:
            print("On réalise les pages individuelles")
        for fiche in self:
            if self.loglevel > 0:
                print("* On réalise la page « {} »".format(fiche.nom))
            nomPage = "{}:{}".format(espaceNommage, fiche.nom)
            page = pywikibot.Page(self.site, nomPage)
            if self.estModifiableAuto(page):
                if self.loglevel > 9: # débogage allégé, on accélère
                    print("loglevel = ", self.loglevel, "on ne modifie pas la page")
                    continue
                t = datetime.now()
                # calcul d'avance afin que les sources de la fiche soient à jour
                text = fiche.__str__(style=self.style)
                page.text = self.marquage(sources=fiche.sources) + text
                page.save()
        return

    def estModifiableAuto(self, page):
        """
        Détermine s'il est opportun de modifier automatiquement
        une page. Renvoie un booleen
        """
        if len(page.text) == 0:
            return True
        auto, version, date = self.litMarquage(page)
        if not auto :
            # la page ne contient pas le marquage automatique,
            # elle a dû être modifiée manuellement.
            return False
        if version < VERSION:
            # la version de fichelogiciel utilisée pour créer la page
            # est trop ancienne, on peut modifier la page
            return True
        if self.csvDate > date:
            # le fichier CSV est plus récent que la date de la page
            # on peut modifier la page
            return True
        return False # par défaut on ne change pas la page

    def litMarquage(self, page):
        """
        Lit des informations dans le marquage d'une page
        puis renvoie comme information : le fait que la page soit automatique,
        la version de fichelogiciel, date et heure
        @return estAuto, version, date
        @rtype Tuple(bool, str, datetime.datetime)
        """
        version = "0"
        date = datetime.fromisoformat('2000-01-01')
        auto = False
        if "    " + DEBUT_MARQUAGE in page.text:
            auto = True
            s = re.search(r"^    {} le (?P<j>\d\d)/(?P<m>\d\d) (?P<a>\d\d\d\d) à (?P<heure>\d\d):(?P<minute>\d\d).*$".format(DEBUT_MARQUAGE), page.text, re.M|re.S)
            if s:
                date = datetime(
                    int(s.group("a")), int(s.group("m")), int(s.group("j")),
                    hour=int(s.group("heure")), minute=int(s.group("minute")))
            s = re.search(r"    avec la bibliothèque «.fichelogiciel.» en version (?P<version>[\.\d]+)$", page.text, re.M|re.S)
            if s:
                version = s.group("version")
            if self.loglevel > 5:
                print("auto = {}, version = {}, date = {}".format(
                    auto,version, date.strftime("%d/%m %Y %H:%M")))
        return auto, version, date

    def marquage(self, sources = []):
        if sources:
            sources = "'''Sources'''\n    \n    " + "\n    ".join(sources)
        else:
            sources=""
        t = datetime.now()
        return """\
    {debut} le {jour} à {heure},
    avec la bibliothèque « fichelogiciel » en version {version}
    voir https://framagit.org/georgesk/freeducwikibot
    {sources}

""".format(
    debut = DEBUT_MARQUAGE,
    jour  = t.strftime("%d/%m %Y"),
    heure = t.strftime("%H:%M"),
    version = VERSION,
    sources = sources
)
