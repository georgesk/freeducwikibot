wget --append-output=freeduc-book.log\
     --recursive \
     --level=2 \
     --no-clobber \
     --page-requisites \
     --adjust-extension \
     --restrict-file-names=windows \
     --accept="*.html.tmp,*.html.tmp.html,*.jpg,*.png,*.css" \
     --ignore-case \
     --domains="web.archive.org" \
     https://web.archive.org/web/20081221175854/http://ofset.sourceforge.net/freeduc/book/book_10.html

#wget --append-output=freeduc-book.log \
#     --continue \
#     --retry-connrefused \
#     --recursive \
#     --level=3 \
#     --convert-links \
#     --accept="jpg,jpeg,png,gif" \
#     --ignore-case \
#     https://web.archive.org/web/20081221175854/http://ofset.sourceforge.net/freeduc/book/book_10.html
