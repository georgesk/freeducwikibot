from pywikibot import family, config

class Family(family.Family):
    name = 'local'
    langs = {
        'fr': 'localhost',
    }
    
    def scriptpath(self, code):
        return '/mediawiki'
