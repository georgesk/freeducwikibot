Freeduc Mediawiki Bot
=====================

Il s'agit d'un automate pour gérer des fiches de logiciels éducatifs,
en faire une partie intégrante d'un mediawiki, afin d'en faciliter les
mises à jour par des contributeurs.

Dépendances de ce projet :
--------------------------

Pour lancer les scripts ici présents, on peut utiliser un système Debian
version, il faut installer en plus la bibliothèque `pywikibot`,
disponible sur le service [Pypi](https://pypi.org/project/pywikibot/).

Versions des dépendances :
* système Debian version 10 ou 11, qui fournit Python3
* pywikibot version 5.6.0

Fonctionnalités en place :
==========================

Pour pouvoir tester les scripts, il faut disposer d'un accès à un mediawiki ;
un fichier `credentials` contient au moins un `tuple` tel que
```("nom d'utilisateur', "mot de passe")```
dans sa première ligne. Pour faire un développement, on peut créer un mediawiki
fonctionnant localement sur son ordinateur. La version testée de Mediawiki
est 1.31.12

Le script `mon_essai.py`
------------------------

Ce script inscrit dans la page "Essai" une exportation des 
[données du projet Primtux](https://ressources.primtux.fr/quel-logiciel-pour-quel-domaine-les-cartes-heuristiques/ "Cliquez ici"), en une dizaine de secondes.

Le script `mon_essai.py` interagit avec un mediawiki
installé localement, accessible par http://localhost/mediawiki/

Les données spéciales à ce mediawiki sont dispersées entre les
fichiers `.pywikibot/families/local_family.py`, le fichier
`user-config.py`, le fichier (non distribué) `credentials`, et quelques
lignes du script lui-même (voir la fonction `login()`)

Le script `export_primtux.py`
-----------------------------

Ce script réalise une exportation des 
[données du projet Primtux](https://ressources.primtux.fr/quel-logiciel-pour-quel-domaine-les-cartes-heuristiques/ "Cliquez ici"), dans plusieurs pages :
* une page de sommaire, nommée `Primtux:Sommaire`
* une page par fiche, dans le même espace de nommage, chacune avec son nom. Par example, la page de la fiche « Abuledu aller » sera nommée `Primtux:Abuledu_aller`.

Ce script travaille plus longtemps : comme il y a une attente de 
10 secondes par page, il faut compter une demi-heure d'attente pour 
parcourir toutes les fiches de Primtux version 6.

Le script `wget-freeduc-book.sh`
--------------------------------

Ce script a permis de récupérer une archive de Frreduc-Book, version 1.4,
conservé par le projet Wayback Machine à l'URL
https://web.archive.org/web/20081221175854/http://ofset.sourceforge.net/freeduc/book/book_10.html

L'archive a été comprimée dans le fichier 
`web-archive-org-freeduc-book-1.4.tgz`.

Le script `restore_freeduc_book.py`
-----------------------------------

L'usage de la commande wget que réalise le script précédent n'est pas très
concluant, du moins dans le cadre de la récupération de données de la
Wayback Machine. Le script `restore_freeduc_book.py` permet de nettoyer
les données reçues par le précédent script, pour en faire un arbre de
fichiers HTML, de fichiers de styles et d'images rassemblés de façon 
cohérente. Cet arbre de fichiers est comprimé dans l'archive
`freeduc-book-1.4.tgz`.
