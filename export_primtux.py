#!/usr/bin/python3
"""
Un script très simple pour pywikibot, permettant d'injecter les pages
de la base de données de Primtux

les données relatives au site à modifier sont en partie dans le fichier 
/home/georgesk/.pywikibot/families/local_family.py, en partie dans
le fichier user-config.py et elles sont complétées par quelques lignes
ci-dessous : config2.register_families_folder(...) et
config.usernames[...

Exemple d'un essai :
--------------------

georgesk@georges:~/developpement/pywikibot$ python3 mon_essai.py
Sleeping for 9.5 seconds, 2021-02-12 23:43:21
Page [[local:fr:Essai]] saved

-------------------------------------------------
Copyright : (c) 2021 Georges Khaznadar <georgesk@debian.org>

Licence: GPL-3+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

from fichelogiciel import FicheLogicielConteneur #,FicheLogiciel 
from wikibot import login, upload
    

if __name__ == "__main__":
    flc = FicheLogicielConteneur(
        fichierCsv    = "primtux/logiciels-PrimTux-V6.csv",
        nomCle        = "LOGICIEL",
        resumeCle     = "Descriptif",
        style         = "table",
        site          = login(),
        propListeCles = [ # les propriétés conttenant des listes
            "Domaine",
            "Matière",
            "Niveau",
            "Session PrimTux",
            "Licence",
        ],
        loglevel = 2
    )
    print(flc)
    flc.sitePlusieursPages("Primtux")
