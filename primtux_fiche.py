#! /usr/bin/python3

"""
Cette bibliothèque permet de récupérer des ressources du site 
http://ressources.primtux.fr/ ; pour une page donnée, ça permet d'en
extraire :
- la description du logiciel plus une copie d'écran
- le guide de démarrage (textes en alternance avec des images
- les données de copyright : qui, quand

-----
dépendance : le paquet libhtml-wikiconverter-mediawiki-perl peut être
utile si on veut convertir self.quide

"""
import requests
import re
import bs4
from datetime import datetime
from subprocess import call, Popen, PIPE

class PrimtuxFiche:

    def __init__(self, url):
        # on peut réessayer trois fois de récupérer une URL
        for i in range(3):
            try:
                req = requests.get(url)
                if req.content:
                    break
            except requests.exceptions.ConnectionError as err:
                if i < 2:
                    print("Attention! ", err)
                else:
                    raise Exception("Troisième essai manqué : "+str(err))
                
            
        self.doc = bs4.BeautifulSoup(req.content.decode("utf-8"), "html.parser")
        self.icon = None
        self.title = None
        self.publicationDate = None
        self.author = None
        self.authorLink = None
        self.ficheTechnique = None
        self.age = None
        self.niveau = None
        self.categorie = None
        self.description = None
        self.descriptionImg = None
        self.guide= None
        self.images = None
        self.parseFiche()
        return

    def parseFiche(self):
        """
        récupère les éléments utiles de la fiche de logiciel éducatif Primtux
        """
        # récupération des lignes du tableau de la fiche
        meta = self.doc.find("div", attrs={"class": "entry-meta"})
        self.publicationDate = datetime.fromisoformat(meta.find("time")["datetime"])
        authorA = meta.find("span",{"class": "author vcard"}).find("a")
        self.author = authorA.text
        self.authorLink = authorA["href"]
        # récupérations des contenus
        content = self.doc.find("div", attrs={"class": "entry-content"})
        self.images = [i["src"] for i in content.find_all("img")]
        rows = content.find_all("tr")
        self.icon = rows[0].find("img")["src"]
        self.title = rows[0].find("strong")
        resume = rows[2].find_all("td")
        self.ficheTechnique = resume[0].text
        s = re.search(r"Age : (.*)\sNiveau : (.*)\sCatégorie : (.*)",self.ficheTechnique, re.M|re.S)
        if s:
            self.age, self.niveau, self.categorie = s.groups()
        self.description = resume[1].text
        self.descriptionImg = None
        description_image = resume[1].find("img")
        if description_image:
            self.descriptionImg=description_image["src"]
        self.guide = "[Pas de guide de l'utilisateur]\n\n"
        if len(rows) >3:
            self.guide = rows[3].find("div",{"class": "panel-body"})
        return

    def html2wiki(self, html):
        """
        Enrobe l'appel à la commande shell html2wiki
        @param html la source HTML
        @return du wikicode
        """
        cmd = "html2wiki --dialect=MediaWiki"
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        p.stdin.write(html.encode("utf-8"))
        out, err = p.communicate()
        out = out.decode("utf-8")
        #mise en place de sauts de lignes pour les images
        out = re.sub(r"\[\s*(http.*)\[\[Image:(.*)\]\]\s*\]",
                     "[\\1[[Image:Primtux-\\2]]]",
                     out)
        return out
        

    def htmlDemo(self):
        """
        Place les attributs dans une présentation HTML
        """
        html = ""
        # l'icône
        html += f"""<h2>Icône</h2><img src="{self.icon}"/>\n"""
        # le titre
        html += f"<h2>Titre</h2>{self.title}\n"
        # meta
        html += f"<h2>À propos</h2><p>publié le {self.publicationDate} par {self.author} <a href='{self.authorLink}'>(lien)</a></p><p><strong>Âge : </strong>{self.age}, <strong>Niveau : </strong>{self.niveau}, <strong>Catégorie : </strong>{self.categorie}</p>\n"
        # resume
        html += f"<h2>Résumé</h2><h3>Fiche technique</h3>{self.ficheTechnique}<h3>Description</h3><img src='{self.descriptionImg}'/><div>{self.description}</div>\n"
        # guide
        html += f"<h2>Guide de prise en main</h2>{self.guide}"
        html += f"""<h3>Wikicode</h3><textarea cols=160 rows=25>{self.html2wiki(self.guide)}</textarea>"""
        # urls des images
        html += """\
<style type="text/css">
  #images td {
    border: 1px solid black;
    border-collapse: collapse;
  }
  #images tr:nth-child(odd) {
    background: lightgrey;
  }
</style>
"""
        html += "<table id ='images'><tr><td>\n"
        html += "</td><td>".join([i.split("/")[-1] for i in self.images])
        html += "</td></tr><tr><td>\n"
        html += "</td><td>".join([f"<img src='{i}' style='max-width: 150px; max-height: 150px;'/>" for i in self.images])
        html += "</td></tr></table>\n"
        return html
        

if __name__ == "__main__":
    import tempfile

    call("rm /tmp/primtux*", shell=True)
    pf = PrimtuxFiche("http://ressources.primtux.fr/2015/11/26/abuledu-calcul-mental/")
    f = tempfile.NamedTemporaryFile(mode="w",encoding="utf-8", prefix="primtux", delete=False)
    f.write(pf.htmlDemo())
    call("firefox {}".format(f.name), shell = True)
